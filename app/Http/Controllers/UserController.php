<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->_response(null, 'Email is a required field');
        }

        $user = User::firstOrCreate([
            'email' => $request->input('email')
        ]);

        return $this->_response($user, 'Cannot find nor create this User');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = User::with('tasks')->where('users.id', '=', $id)->first();

        return $this->_response($user, "Cannot find User (ID=$id)");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with('tasks')->where('users.id', '=', $id)->first();

        if (!$user)
            return $this->_response(null,"Cannot find User (ID=$id)");

        if ($request->has('name'))
            $user->name = $request->input('name');

        if ($request->has('email'))
            $user->email = $request->input('email');

        if ($request->has('name') || $request->has('email'))
            $user->save();

        return $this->_response($user, "Cannot update User (ID=$id)");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
