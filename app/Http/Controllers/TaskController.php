<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $task = Task::create([
            'user_id' => $request->input('user_id'),
            'description' => $request->input('description')
        ]);

        return $this->_response($task, 'Could not create new Task');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $task = Task::find($id);

        return $this->_response($task, "Could not find Task (ID=$id)");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        if (!$task)
            return $this->_response(null, "Could not find Task (ID=$id)");

        if ($request->has('description'))
            $task->description = $request->input('description');

        if ($request->has('completed_at')) {
            if ($request->input('completed_at'))
                $task->completed_at = Carbon::now();
            else
                $task->completed_at = null;
        }

        if ($request->has('description') || $request->has('completed_at'))
            $task->save();

        return $this->_response($task, "Could not update Task (ID=$id)");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);

        if (!$task)
            return $this->_response(null, "Could not find Task (ID=$id)");

        $task->delete();

        return $this->_response((object)[
            'id' => $id
        ]);
    }
}
