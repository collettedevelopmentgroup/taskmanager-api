<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function testCreateUserValidation()
    {
        $this->json('POST', '/api/users', [])
            ->seeJsonStructure([
                'status',
                'response' => [
                    'message'
                ]
            ])
            ->seeJson([
                'status' => 'ERROR',
                'message' => "Email is a required field"
            ]);
    }

    public function testCreateUser()
    {
        $this->json('POST', '/api/users', [
            'email' => 'testemail@noemail.net'
        ])
            ->seeJsonStructure([
                'status',
                'response' => [
                    'email',
                    'id'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'email' => 'testemail@noemail.net'
            ]);
    }

    public function testGetUser()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $this->get("/api/users/{$user->id}")
            ->seeJsonStructure([
                'status',
                'response' => [
                    'id',
                    'email',
                    'name',
                    'tasks'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'id' => $user->id
            ]);
    }

    public function testUpdateUser()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $this->put("/api/users/{$user->id}", [
            'name' => 'test name'
        ])
            ->seeJsonStructure([
                'status',
                'response' => [
                    'id',
                    'email',
                    'name',
                    'tasks'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'id' => $user->id,
                'name' => 'test name'
            ]);
    }
}