<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;

class TaskTest extends TestCase
{
    use DatabaseMigrations;

    public function testCreateTask()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $this->json('POST', '/api/tasks', [
            'user_id' => $user->id,
            'description' => 'This is my test case Task!'
        ])
            ->seeJsonStructure([
                'status',
                'response' => [
                    'user_id',
                    'description',
                    'id'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'user_id' => $user->id,
                'description' => 'This is my test case Task!'
            ]);
    }

    public function testGetTask()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $response = $this->call('POST', '/api/tasks', ['user_id' => $user->id, 'description' => "I'm testing the capabilities of this task!"]);
        $task = json_decode($response->getContent())->response;

        $this->json('GET', "/api/tasks/{$task->id}")
            ->seeJsonStructure([
                'status',
                'response' => [
                    'id',
                    'user_id',
                    'description'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'id' => $task->id,
                'user_id' => $user->id,
                'description' => $task->description
            ]);
    }

    public function testGetTaskFail()
    {
        $this->json('GET', '/api/tasks/0')
            ->seeJsonEquals([
                'status' => 'ERROR',
                'response' => [
                    'message' => "Could not find Task (ID=0)"
                ]
            ]);
    }

    public function testUpdateTask()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $response = $this->call('POST', '/api/tasks', ['user_id' => $user->id, 'description' => "I'm testing the capabilities of this task!"]);
        $task = json_decode($response->getContent())->response;

        $this->json('PUT', "api/tasks/{$task->id}", [
            'description' => "I've updated this task!",
            'completed_at' => true
        ])
            ->seeJsonStructure([
                'status',
                'response' => [
                    'id',
                    'user_id',
                    'description'
                ]
            ])
            ->seeJson([
                'status' => 'SUCCESS',
                'id' => $task->id,
                'user_id' => $user->id,
                'description' => "I've updated this task!"
            ]);
    }

    public function testUpdateTaskFail()
    {
        $this->json('PUT', '/api/tasks/0')
            ->seeJsonEquals([
                'status' => 'ERROR',
                'response' => [
                    'message' => "Could not find Task (ID=0)"
                ]
            ]);
    }

    public function testDeleteTask()
    {
        $response = $this->call('POST', '/api/users', ['email' => 'emailtest@noemail.net']);
        $user = json_decode($response->getContent())->response;

        $response = $this->call('POST', '/api/tasks', ['user_id' => $user->id, 'description' => "I'm testing the capabilities of this task!"]);
        $task = json_decode($response->getContent())->response;

        $this->json('DELETE', "/api/tasks/{$task->id}")
            ->seeJsonEquals([
                'status' => 'SUCCESS',
                'response' => [
                    'id' => "{$task->id}"
                ]
            ]);
    }

    public function testDeleteTaskFail()
    {
        $this->json('DELETE', '/api/tasks/0')
            ->seeJsonEquals([
                'status' => 'ERROR',
                'response' => [
                    'message' => "Could not find Task (ID=0)"
                ]
            ]);
    }
}